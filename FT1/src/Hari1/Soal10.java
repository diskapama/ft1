package Hari1;
import java.util.Scanner;
public class Soal10 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		// Deklarasi
				System.out.print("Inputkan Kata : ");
				String abjad = input.nextLine();
				String replace = abjad.replace(" ", "").toLowerCase();
				String[] aAbjad = replace.split("");
				char[] aChar = new char[aAbjad.length];
				String vokal = "";
				String konsonan = "";
				String other = "";

				// Fill
				for (int i = 0; i < aChar.length; i++) {
					aChar[i] = aAbjad[i].charAt(0);
				}

				// First
				// Looping Array Cara Panjang
				for ( char j = 0; j < aChar.length; j++) {
					System.out.print(aChar[j] + " ");
				}
				// Looping Array Cara pendek
//				for (char s : aChar) {
//					System.out.print(s + " ");
//				}
				
				System.out.println();
				
				// Short
				for (int x = 0; x < aChar.length; x++) {
					for (int i = 0; i < aAbjad.length - 1; i++) {
						char wadah = aChar[i]; // wadah : D, I, S, S
						if (aChar[i] > aChar[i + 1]) {
							aChar[i] = aChar[i + 1]; //aChar : S=K, S=A
							aChar[i + 1] = wadah; //aChar[i+1] : K=S A=S
						}
					}
				}
				
				for (int i = 0; i < aChar.length; i++) {
					switch (aChar[i]) {
					case 'a':
						vokal = vokal + aChar[i];
						break;
					case 'i':
						vokal = vokal + aChar[i];
						break;
					case 'u':
						vokal = vokal + aChar[i];
						break;
					case 'e':
						vokal = vokal + aChar[i];
						break;
					case 'o':
						vokal = vokal + aChar[i];
						break;
					case '0':
						vokal = vokal + aChar[i];
						break;
						
					case '1':
						other = other + aChar[i];
						break;
					case '2':
						other = other + aChar[i];
						break;
					case '3':
						other = other + aChar[i];
						break;
					case '4':
						other = other + aChar[i];
						break;
					case '5':
						other = other + aChar[i];
						break;
					case '6':
						other = other + aChar[i];
						break;
					case '7':
						other = other + aChar[i];
						break;
					case '8':
						other = other + aChar[i];
						break;
					case '9':
						other = other + aChar[i];
						break;
						
					case '!':
						other = other + aChar[i];
						break;
					case '@':
						other = other + aChar[i];
						break;
					case '#':
						other = other + aChar[i];
						break;
					case '$':
						other = other + aChar[i];
						break;
					case '%':
						other = other + aChar[i];
						break;
					case '^':
						other = other + aChar[i];
						break;
					case '&':
						other = other + aChar[i];
						break;
					case '*':
						other = other + aChar[i];
						break;
					case '(':
						other = other + aChar[i];
						break;
					case ')':
						other = other + aChar[i];
						break;
					case '-':
						other = other + aChar[i];
						break;
					case '+':
						other = other + aChar[i];
						break;
					case '~':
						other = other + aChar[i];
						break;
					case '_':
						other = other + aChar[i];
						break;
					case '=':
						other = other + aChar[i];
						break;
						
					default:
						konsonan = konsonan + aChar[i];
						break;
					}
				}
				
				System.out.println();
				
				System.out.println("Huruf Vokal : " + vokal);
				System.out.println("Huruf Konsonan : " + konsonan);
				System.out.println("Huruf Konsonan : " + other);

				// Second
//				System.out.println();
//				for (char i = 0; i < aChar.length; i++) {
//					System.out.print(aChar[i] + " ");
//				}
//				for (char c : aChar) {
//					System.out.print(c + " ");
//				}
		
		input.close();

	}

} //
