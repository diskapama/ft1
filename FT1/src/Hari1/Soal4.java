package Hari1;

import java.util.Scanner;

public class Soal4 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		// Jam 12
		System.out.print("Input Jam (Format hh:mm:ddAM) : ");
		String amPm = input.nextLine();
//		String amPm = "12:35:00PM";

		// get Hour
		int hh = Integer.parseInt(amPm.substring(0, 2));

		// Cek am or pm
		if (amPm.charAt(5) == 'A') {
			if (hh >= 13) { // Jika jam lebih dari sama dengan 13 Maka tidak valid karena AM max sampai jam
							// 12
				System.out.print("Jam Tidak Valid");
				// am
			} else if (hh == 12) {
				System.out.print("00");
				// cetak mm dan ss
				System.out.print(amPm.substring(2, 5));
			} else if (hh == 00) {
				System.out.print("12");
				// cetak mm dan ss
				System.out.print(amPm.substring(2, 5));
			} else {
				System.out.println(amPm.substring(0, 5));
			}
		} else if (amPm.charAt(5) == 'P') {
			// pm
			if (hh >= 13) {
				hh = hh - 12;
				System.out.print(hh);
				System.out.print(amPm.substring(2, 5));
			} else if (hh == 12) {
				// jam
				System.out.print("12");
				// cetak mm dan ss
				System.out.print(amPm.substring(2, 5));
			} else {
				hh = hh + 12;
				System.out.print(hh);
				System.out.print(amPm.substring(2, 5));
			}

		} else if (amPm.charAt(5) == '-'){
			if (hh >= 13) { // Jika jam lebih dari sama dengan 13 Maka tidak valid karena AM max sampai jam
				// 12
				hh = hh - 12;
				System.out.print(hh);
				System.out.print(amPm.substring(2, 5) + "PM");
				// am
			} else if (hh == 12) {
				System.out.print("12");
				// cetak mm dan ss
				System.out.print(amPm.substring(2, 5) + "PM");
			} else if (hh == 00) {
				System.out.print("12");
				// cetak mm dan ss
				System.out.print(amPm.substring(2, 5) + "AM");
			} else {
				System.out.println(amPm.substring(0, 5) + "AM");
			}
		}

		input.close();
	}

} //
